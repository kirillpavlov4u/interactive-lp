"use strict";

document.addEventListener("DOMContentLoaded", () => {
	setServiceTabContent();
	setWorkTabContent();
	
	document.addEventListener("click", (event) => {
		if (event.target.classList.contains("services-tab")) {
			changeServiceTab(event.target);
		}
		
		if (event.target.classList.contains("works-tab")) {
			changeWorksTab(event.target);
		}
		
		if (event.target.id === "loadMoreWorks") {
			loadMoreWorks(event.target);
		}
		
		
	});
});

/* Code For "Our Services" Section */

const SERVICES_TABS = {
		servicesWebDesign: {
			imgSrc: "img/our-services/web-design.jpg",
			text: "Web design encompasses many different skills and disciplines in the production and maintenance of websites. The different areas of web design include web graphic design; interface design; authoring, including standardised code and proprietary software; user experience design; and search engine optimization."
		},
		servicesGraphicDesign: {
			imgSrc: "img/our-services/graphic-design.jpg",
			text: "Graphic design is the process of visual communication and problem-solving through the use of typography, photography, iconography and illustration. The field is considered a subset of visual communication and communication design, but sometimes the term \"graphic design\" is used synonymously."
		},
		servicesOnlineSupport: {
			imgSrc: "img/our-services/support.jpg",
			text: "Online help is topic-oriented, procedural or reference information delivered through computer software. It is a form of user assistance. Most online help is designed to give assistance in the use of a software application or operating system, but can also be used to present information on a broad range of subjects."
		},
		servicesAppDesign: {
			imgSrc: "img/our-services/app-design.jpg",
			text: "Mobile app development is the act or process by which a mobile app is developed for mobile devices, such as personal digital assistants, enterprise digital assistants or mobile phones. These applications can be pre-installed on phones during manufacturing platforms, or delivered as web applications using server-side or client-side processing (e.g., JavaScript) to provide an \"application-like\" experience within a Web browser."
		},
		servicesOnlineMarketing: {
			imgSrc: "img/our-services/online-marketing.jpg",
			text: "Digital marketing is the component of marketing that utilizes internet and online based digital technologies such as desktop computers, mobile phones and other digital media and platforms to promote products and services. Its development during the 1990s and 2000s, changed the way brands and businesses use technology for marketing."
		},
		servicesSeoService: {
			imgSrc: "img/our-services/seo.png",
			text: "Search engine optimization (SEO) is the process of growing the quality and quantity of website traffic by increasing the visibility of a website or a web page to users of a web search engine.[1] SEO refers to the improvement of unpaid results (known as \"natural\" or \"organic\" results) and excludes direct traffic and the purchase of paid placement."
		},
	};

function changeServiceTab (tab) {
	const image = document.getElementById('ourServiceImg');
	const description = document.getElementById('ourServiceText');
	const allServiceTabs = document.getElementsByClassName("services-tab");
	
	for (let i in allServiceTabs) {
		if (i < allServiceTabs.length)
			allServiceTabs[i].classList.remove("active");
	}
	
	tab.classList.add("active");	
	
	image.src = SERVICES_TABS[tab.id].imgSrc;
	description.textContent = SERVICES_TABS[tab.id].text;
}

function setServiceTabContent () {
	const serviceTabs = document.querySelector(".services-tab.active");
	
	changeServiceTab(serviceTabs);
}

/* Code For "Our Amazing Works" Section */

const WORKS_TABS = {
		all: {
			category: ["webDesign","graphicDesign","landingPage","wordPress"]
		},
		webDesign: {
			imgSrc: "img/web-design/web-design",
			imgCounter: 7
		},
		graphicDesign: {
			imgSrc: "img/graphic-design/graphic-design",
			imgCounter: 12
		},
		landingPage: {
			imgSrc: "img/landing-page/landing-page",
			imgCounter: 7
		},
		wordPress: {
			imgSrc: "img/wordpress/wordpress",
			imgCounter: 10
		}, 
	};

function changeWorksTab (tab) {
	const	allWorkTabs = document.getElementsByClassName("works-tab");
	const imagesContainer = document.getElementsByClassName("images-container")[0];
	
	if (tab.classList.contains("active") && countOpenedImages(imagesContainer) > 12) {
		return;
	}
	
	clearImages();
	
	if (countOpenedImages(imagesContainer) < 36) {
		document.getElementById("loadMoreWorks").hidden = false;
	}
	
	function clearImages () {
		imagesContainer.childNodes.forEach(img => {
			if (img.nodeName === "DIV") {
				img.remove();
				clearImages();
			}
		});
	}
	
	for (let i in allWorkTabs) {
		if (i < allWorkTabs.length)
			allWorkTabs[i].classList.remove("active");
	}
	
	tab.classList.add("active");
	
	if (tab.id !== "all") {
		imagesContainer.prepend(createImages(tab));
	}
	
	if (tab.id === "all"){
		imagesContainer.prepend(uploadAllImages(tab));
	}
}

function createImages (tab) {
		const images = document.createDocumentFragment();
	
		for (let i = 1; i <= WORKS_TABS[tab.id].imgCounter; i++) {
			const img = document.createElement("IMG");
			const div = document.createElement("DIV");
			
			div.classList.add("worksImageWrapper");
			img.alt = tab.id;
			img.classList.add("works-image");
			img.src = WORKS_TABS[tab.id].imgSrc + i + ".jpg";
			
			const pop_Up = addPopUpLink(img);		
			
			div.append(img);
			div.append(pop_Up);
			images.appendChild(div);
		}
		return images;
}

function uploadAllImages (tab) {
	const images = document.createDocumentFragment();
	const allTabs = WORKS_TABS[tab.id].category;
	let counter = 0;
	
	for (let i = 0; i < allTabs.length; i++) {
		
		for (let k = 1; k <= WORKS_TABS[allTabs[i]].imgCounter; k++) {
			counter++;
			const img = document.createElement("IMG");
			const div = document.createElement("DIV");
			
			div.classList.add("worksImageWrapper");
			img.alt =	allTabs[i];
			img.classList.add("works-image");
			img.src = WORKS_TABS[allTabs[i]].imgSrc + k + ".jpg";
			
			const pop_Up = addPopUpLink(img);	
			
			div.append(img);
			div.append(pop_Up);
			
			if (counter > 12) {
				div.hidden = true;
			}
			
			images.appendChild(div);
		}
	}
	return images;
}


function setWorkTabContent () {
	const workTabs = document.querySelector(".works-tab.active");
	changeWorksTab(workTabs);
}

function addPopUpLink (img) {
	const popUp = document.getElementById("popUp");
	const popUpNodes = popUp.childNodes;

	popUpNodes.forEach(elem => {
		if (elem.nodeName === "P" && elem.classList.contains("image-category")) {
			switch (img.alt) {
				case "webDesign":
					elem.textContent = "Web Design";
					break;
				case "graphicDesign":
					elem.textContent = "Graphic Design";
					break;
				case "landingPage":
					elem.textContent = "Landing Page";
					break;
				case "wordPress":
					elem.textContent = "WordPress";
					break;
			}
		}
	});
	
	const popUpClone = popUp.cloneNode(true);
	popUpClone.id = "";
	
	return popUpClone;
}

function loadMoreWorks (btn) {
	const imagesContainer = document.getElementsByClassName("images-container")[0];
	let imageContainerCounter = 0;
	
	if (all.classList.contains("active")) {
		imageContainerCounter = countOpenedImages(imagesContainer);
		
		if (imageContainerCounter < 37) {
			let counter = 0;
			imagesContainer.childNodes.forEach(div => {
			if (div.nodeName === "DIV" && counter < 12)
				if (div.hidden === true) {
					div.hidden = false;
					counter++;
				}
			});
		}	
		
		imageContainerCounter = countOpenedImages(imagesContainer);
	}
	
	if (imageContainerCounter === 36) {
		btn.hidden = true;
	}
}

function countOpenedImages (imagesContainer) {
	let imageContainerCounter = 0;
	
	imagesContainer.childNodes.forEach(div => {
		if (div.nodeName === "DIV")
			if (div.hidden === false)
				imageContainerCounter++;
	});
	
	return imageContainerCounter;
}

/* Code For "Image Gallery" Section */

